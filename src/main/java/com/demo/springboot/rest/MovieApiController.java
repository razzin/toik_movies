package com.demo.springboot.rest;

import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.services.impl.MovieListServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class MovieApiController {
    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);

    private MovieListServiceImpl movie_list = new MovieListServiceImpl();

    @PostMapping("/movies")
    public ResponseEntity<Void> createMovie(@RequestBody MovieDto movie) throws URISyntaxException {
        LOG.info("--- title: {}", movie.getTitle());
        LOG.info("--- year: {}", movie.getYear());
        LOG.info("--- image: {}", movie.getImage());
        if(movie.getImage().isEmpty() || movie.getYear() == null || movie.getTitle().isEmpty()){
            return ResponseEntity.badRequest().build();
        }
        else {
            movie_list.addMovie(movie);
            return ResponseEntity.created(new URI("/movies/" + movie.getMovieId())).build();
        }
    }

    @GetMapping("/movies")
    public ResponseEntity<List<MovieDto>> getMovies() {
        LOG.info("--- get all movies: {}", movie_list.getMovies().size());
        LOG.info(movie_list.toString());

        return ResponseEntity.ok().body(movie_list.getMovies());    // = new ResponseEntity<>(movies, HttpStatus.OK);
    }

    @PutMapping("/movies/{id}")
    public ResponseEntity<Void> updateMovie(@PathVariable("id") Integer id, @RequestBody MovieDto movie) {
        LOG.info("--- id: {}", id);
        if(movie_list.updateMovie(id, movie.getTitle(), movie.getYear(), movie.getImage())) {
            return ResponseEntity.ok().build();
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/movies/{id}")
    public ResponseEntity<Void> deleteMovie(@PathVariable("id") Integer id, @RequestBody MovieDto movie) {
        LOG.info("--- id: {}", id);
        if(movie_list.deleteMovie(id, movie.getTitle(), movie.getYear(), movie.getImage())) {
            return ResponseEntity.ok().build();
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

}
