package com.demo.springboot.services;

import com.demo.springboot.dto.MovieDto;

import java.util.List;

public interface MovieListService {

    public List<MovieDto> getMovies();
}
