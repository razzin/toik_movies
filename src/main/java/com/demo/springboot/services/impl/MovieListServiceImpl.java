package com.demo.springboot.services.impl;

import com.demo.springboot.dto.MovieDto;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MovieListServiceImpl {
    private List<MovieDto> movies = new ArrayList<>();
    private int count=1;
    Comparator<MovieDto> compareById = Comparator.comparing(MovieDto::getMovieId);

    public MovieListServiceImpl() {
        movies.add(new MovieDto(1,
                "Piraci z Krzemowej Doliny",
                1999,
                "https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg")
        );
    }

    public void addMovie(MovieDto movie) {
        movie.setMovieId(++count);
        movies.add(movie);
    }

    public List<MovieDto> getMovies() {
        movies.sort(compareById.reversed());
        return movies;
    }

    public boolean updateMovie(Integer id, String title, int year, String image){
        for (MovieDto movie : movies) {
            if (movie.getMovieId().equals(id)) {
                movie.setTitle(title);
                movie.setYear(year);
                movie.setImage(image);
                return true;
            }
        }
        return false;
    }
    public boolean deleteMovie(Integer id, String title, int year, String image){
        for (MovieDto movie : movies) {
            if (movie.getMovieId().equals(id)) {
                movies.remove(movie);
                count--;
                return true;
            }
        }
        return false;
    }


    @Override
    public String toString() {

        return "[" + getMovies().stream().sorted(compareById.reversed())
                .map(MovieDto::getTitle)
                .collect(Collectors.joining(",")) + "]";
    }
}
